var pool = require('./databaseConfig.js');
var adminDB = {
    getAdminbyId: function(username, callback){
        pool.getConnection(function(err,conn){
            if(err){
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Connn !!!");
                var sql = 'SELECT * FROM tb_admin WHERE username = ?';
                conn.query(sql, [username], function(err, result){
                    conn.release();
                    if(err){
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
};
module.exports = adminDB