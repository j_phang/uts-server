var pool = require('./databaseConfig');
var filmDB = {
    getFilm: function (callback){
        pool.getConnection(function(err, conn){
            if(err){
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Connected !");
                var sql = 'SELECT * FROM tb_film';
                conn.query(sql, function(err, result){
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    addFilm: function (judul, images, durasi, tahun, sutradara, callback){
        pool.getConnection(function(err,conn){
            if(err){
                console.log(err);
                return callback(err,null);
            } else {
                console.log("Connected !");
                var sql = 'INSERT INTO tb_film (id, judul, images, durasi, tahun, sutradara) VALUES (NULL, ?, ?, ?, ?, ?)';
                conn.query(sql, [judul, images, durasi, tahun, sutradara], function (err, result){
                    conn.release();
                    if(err){
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    deleteFilm: function (id, callback){
        pool.getConnection(function(err,conn){
            if(err){
                console.log(err);
                return callback (err, null);
            } else {
                console.log("Conn !");
                var sql = 'DELETE FROM tb_film WHERE id=?';
                conn.query(sql, [id], function(err,result){
                    conn.release();
                    if(err){
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    updateFilm: function(judul, images,durasi, tahun, sutradara, id, callback){
        pool.getConnection(function(err, conn){
            if(err){
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Conn ~!");
                console.log(judul+ ", "+images+ ", "+durasi+ ", "+tahun+ ", "+sutradara+ ", "+id)
                var sql = 'UPDATE tb_film SET judul = ?, images=?, durasi=?, tahun=?, sutradara=? WHERE id=?';
                conn.query(sql, [judul,images,durasi,tahun,sutradara,id], function(err, result){
                    conn.release();
                    if(err){
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },

    getFilmbyID: function (id, callback){
        pool.getConnection(function(err, conn){
            if(err){
                console.log(err);
                return callback(err, null);
            } else {
                console.log("Connected !");
                var sql = 'SELECT * FROM tb_film WHERE id = ?';
                conn.query(sql, id, function(err, result){
                    conn.release();
                    if (err){
                        console.log(err);
                        return callback(err, null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }
};
module.exports = filmDB;