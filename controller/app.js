
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var path = require('path');

var cors = require("cors");
var cor = cors();
app.use(cor);
app.use(express.static(path.join(__dirname, "../public")));

var film = require('../model/film.js');
var admin = require('../model/admin.js');

app.get('/api/film', function(req, res){
    film.getFilm(function(err, result){
        if(!err) {
            res.send(result);
        } else {
            console.log (err);
            res.status(500).send(err.code);
        }
    });
});

app.get('/api/filmid/:id', function(req, res){
    var id = req.params.id;
    film.getFilmbyID(id, function(err, result){
        if(!err) {
            res.send(result);
        } else {
            console.log (err);
            res.status(500).send(err.code);
        }
    });
});

app.post('/api/filming', urlencodedParser, jsonParser, function(req, res){
    var judul = req.body.judul;
    var images = req.body.images;
    var durasi = req.body.durasi;
    var tahun = req.body.tahun;
    var sutradara = req.body.sutradara;
    
    film.addFilm(judul, images, durasi, tahun, sutradara, function(err, result){
        if(!err){
            console.log(result);
            res.send(result.affectedRows + ' record ditambahkan');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});

app.delete('/api/filming/:id', function(req,res){
    var id = req.params.id;

    film.deleteFilm(id, function(err,result){
        if(!err){
            console.log(result);
            res.send(result.affectedRows + ' record dihapus');
        } else {
            console.log(err);
            res.status(500).send(err.code);
        }
    });
});

app.post('/api/filming/:id', urlencodedParser, jsonParser, function(req, res){
    var judul = req.body.judul;
    var images = req.body.images;
    var durasi = req.body.durasi;
    var tahun = req.body.tahun;
    var sutradara = req.body.sutradara;
    var id = req.params.id;

    film.updateFilm(judul, images,durasi, tahun, sutradara, id, function(err, result){
        if(!err){
            console.log(result);
            res.send(result.affectedRows + ' record diubah');
        } else {
            console.log(err);
            res.send(err);
        }
    });
});

app.get('/api/admin/:username', function(req, res){
    var username = req.params.username;
    admin.getAdminbyId(username, function(err,result){
        if(!err){
            res.send(result);
        } else {
            console.log(err);
            res.status(500).send(err);
        }
    });
});

module.exports = app